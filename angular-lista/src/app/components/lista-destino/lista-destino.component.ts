import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { DestinoApiCliente } from './../../models/destino-api-cliente.model';
import { Store} from '@ngrx/store';
import { AppState } from '../../app.module';
import { NuevoDestinoAction, ElegidoFavoritoAction } from './../../models/destino-viaje-state.model';


@Component({
  selector: 'app-lista-destino',
  templateUrl: './lista-destino.component.html',
  styleUrls: ['./lista-destino.component.css']
})
export class ListaDestinoComponent implements OnInit {
@Output() onItemAdded : EventEmitter<DestinoViaje>;
destinos;
actualizar: string[];
all;
  constructor(private destinoApiCliente:DestinoApiCliente, private store: Store<AppState>) { 
    this.onItemAdded = new EventEmitter();
    this.actualizar = [];
    this.store.select(state => state.destinos.favorito)
    .subscribe(d=> {
      if(d != null){
        this.actualizar.push('Se ha agregado ' + d.nombre);
      }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items)
  }

  ngOnInit(): void {
  }

  agregado(d:DestinoViaje){
   this.destinoApiCliente.add(d);
    this.onItemAdded.emit(d);
   this.store.dispatch(new NuevoDestinoAction(d));
  }

  elegido(d:DestinoViaje){
    this.destinoApiCliente.elegir(d);
    this.store.dispatch(new ElegidoFavoritoAction(d));
  }
}
