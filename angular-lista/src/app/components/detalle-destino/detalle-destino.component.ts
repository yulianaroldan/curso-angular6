import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import {DestinoApiCliente} from './../../models/destino-api-cliente.model';
import { DestinoViaje } from 'src/app/models/destino-viaje.model';


@Component({
  selector: 'app-detalle-destino',
  templateUrl: './detalle-destino.component.html',
  styleUrls: ['./detalle-destino.component.css']
})
export class DetalleDestinoComponent implements OnInit {
  destino:DestinoViaje;
  style={
    sources:{
      world:{
        type:'geojson',
        data: 'https://raw.githubusercontent.com/johan/world.geo.json/master/countries.geo.json'
      }
    },
    version: 8,
    layers:[{
      'id':'countries',
      'type': 'fill',
      'source':'world',
      'layout':{},
      'paint':{
        'fill-color':'#6F788A'
      }
    }]
  };

  constructor(private route:ActivatedRoute, private destinosApiClient:DestinoApiCliente) { }

  ngOnInit() {
    const id = this.route.snapshot.paramMap.get('id');
    this.destino = this.destinosApiClient.getById(id);
  }

}
