import { Component, OnInit, Input, HostBinding, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { Store } from '@ngrx/store';
import { AppState } from '../../app.module';
import { VoteUpAction, VoteDownAction } from './../../models/destino-viaje-state.model';
import { trigger, state, style, transition, animate} from '@angular/animations';

@Component({
  selector: 'app-lista',
  templateUrl: './lista.component.html',
  styleUrls: ['./lista.component.css'],
  animations: [
    trigger('esFavorito', [
      state('estadoFavorito', style({
        backgroundColor:'PaleTurquoise'
      })),
      state('estadoNoFavorito', style({
        backgroundColor: 'WhiteSmoke'
      })),
      transition('estadoNofavorito => estadoFavorito', [
        animate('3s')
      ]),
      transition('estadoFavorito => estadoNoFavorito',[
        animate('1s')
      ]),
    ])
  ]
})
export class ListaComponent implements OnInit {
  @Input() destino:DestinoViaje;
  @Output() elegir:EventEmitter<DestinoViaje>;
  @Input() posicion:number;
  @HostBinding('attr.class') cssClass='col-md-4';

  constructor(private store: Store<AppState>) {
    this.elegir = new EventEmitter();
   }

  ngOnInit(): void {
  }

  ir(){
    this.elegir.emit(this.destino);
    return false;
  }

  voteUp(){
    this.store.dispatch(new VoteUpAction(this.destino));
    return false;
  }
  
  voteDown(){
    this.store.dispatch(new VoteDownAction(this.destino));
    return false;
  }
}
