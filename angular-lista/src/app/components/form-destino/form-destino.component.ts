import { Component, OnInit, Output, EventEmitter, Inject, forwardRef } from '@angular/core';
import { DestinoViaje } from './../../models/destino-viaje.model';
import { FormGroup, FormBuilder, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { fromEvent } from 'rxjs';
import { APP_CONFIG, AppConfig } from 'src/app/app.module';

@Component({
  selector: 'app-form-destino',
  templateUrl: './form-destino.component.html',
  styleUrls: ['./form-destino.component.css']
})
export class FormDestinoComponent implements OnInit {
  @Output() onItemAdded : EventEmitter<DestinoViaje>;
  fg: FormGroup;
  minLongitud = 5;
  public minLong = 15;
  resultado: string[];

  constructor(fb:FormBuilder, @Inject(forwardRef(()=> APP_CONFIG)) private config: AppConfig) { 
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      nombre:['', Validators.compose([Validators.required, this.nombreValidadorParametrizable(this.minLongitud)])],
      imagenURL:['',  Validators.compose([Validators.required,  this.nombreValidado])]
    });

    this.fg.valueChanges.subscribe((form:any)=>{
      console.log('cambio el formulario: ', form);
    });

    this.fg.controls['nombre'].valueChanges.subscribe((value:string)=>{
      console.log('nombre cambio: ', value);
    })
    }

  ngOnInit(): void {
    let elemento = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elemento, 'input')
    .pipe(
      map((e:KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 2),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(AjaxResponse => {
      this.resultado = AjaxResponse.response;
    });
  }

  guardar(nombre:string, imagenURL:string):boolean{
    const d = new DestinoViaje(nombre, imagenURL);
    this.onItemAdded.emit(d);
    return false;
  }

  nombreValidador(control: FormControl):{[s: string]:boolean}{
    const l = control.value.toString().trim().length;
    if(l>0 && l < 5){
      return {invalidNombre: true};
    }
    return null;
  }

  nombreValidado(control: FormControl):{[s: string]:boolean}{
    let l = control.value.toString().trim().length;
    if(l>0 && l < 15){
      return {invalid: true};
    }
    return null;
  }

  nombreValidadorParametrizable(minLong:number):ValidatorFn{
    return (control:FormControl):{[s:string]:boolean} | null =>{
      const l = control.value.toString().trim().length;
      if(l>0 && l< 5){
        return {minLongNombre: true};
      }
      return null;
    }
      return null;
    }
  }
