import { BrowserModule } from '@angular/platform-browser';
import { NgModule, InjectionToken, Injectable, APP_INITIALIZER } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';
import { DestinosViajesState, reducerDestinosViajes, intializeDestinosViajesState, DestinosViajesEffects, InitMyDataAction } from './models/destino-viaje-state.model';
import { ActionReducerMap, Store } from '@ngrx/store';
import { StoreModule as NgRxStoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import Dexie from 'dexie';
import { TranslateModule, TranslateLoader} from '@ngx-translate/core';
import { NgxMapboxGLModule} from 'ngx-mapbox-gl';
import { BrowserAnimationsModule} from '@angular/platform-browser/animations';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ListaComponent } from './components/lista/lista.component';
import { ListaDestinoComponent } from './components/lista-destino/lista-destino.component';
import { DetalleDestinoComponent } from './components/detalle-destino/detalle-destino.component';
import { DestinoApiCliente} from './models/destino-api-cliente.model';
import { HttpClientModule, HttpClient, HttpHeaders, HttpRequest } from '@angular/common/http'

import { FormDestinoComponent } from './components/form-destino/form-destino.component';
import { LoginComponent } from './components/login/login/login.component';
import { ProtectedComponent } from './components/protected/protected/protected.component';
import { AuthService } from './services/auth.service';
import { UsuarioLogueadoGuard} from './guards/usuario-logueado/usuario-logueado.guard';
import { VuelosComponentComponent } from './components/vuelos/vuelos-component/vuelos-component.component';
import { VuelosMainComponentComponent } from './components/vuelos/vuelos-main-component/vuelos-main-component.component';
import { VuelosMasInfoComponentComponent } from './components/vuelos/vuelos-mas-info-component/vuelos-mas-info-component.component';
import { VuelosDetalleComponentComponent } from './components/vuelos/vuelos-detalle-component/vuelos-detalle-component.component';
import { ReservasModule } from './reservas/reservas.module';
import { DestinoViaje } from './models/destino-viaje.model';
import { getTranslationDeclStmts } from '@angular/compiler/src/render3/view/template';
import { Observable, from } from 'rxjs';
import { flatMap } from 'rxjs/operators';
import { EspiameDirective } from './espiame.directive';
import { TrackearClickDirective } from './trackear-click.directive';

// app config
export interface AppConfig{
  apiEndpoint:string;
}
const APP_CONFIG_VALUE: AppConfig = {
  apiEndpoint: 'http://localhost:3000'
};
export const APP_CONFIG = new InjectionToken<AppConfig>('app.config');
// fin config

export const childrenRoutesVuelos:Routes=[
  {path:'',redirectTo:'main', pathMatch:'full'},
{path:'main',component:VuelosMainComponentComponent},
{path:'mas-info', component: VuelosMasInfoComponentComponent},
{path:':id', component:VuelosDetalleComponentComponent}
];  

const routes:Routes=[
  {path:'',redirectTo:'home', pathMatch:'full'},
{path:'home',component:ListaDestinoComponent},
{path:'destino', component: DetalleDestinoComponent},
{path:'login', component:LoginComponent},
{path:'protected', component:ProtectedComponent, canActivate:[UsuarioLogueadoGuard]},
{path:'vuelos', component:VuelosComponentComponent, canActivate:[UsuarioLogueadoGuard],
children:childrenRoutesVuelos}
];

//redux init 
export interface AppState{
  destinos:DestinosViajesState;
}
const reducers : ActionReducerMap<AppState> ={
  destinos: reducerDestinosViajes
};

let reducersInitialState ={
  destinos: intializeDestinosViajesState()
};

//app Init
export function init_app(appLoadService: AppLoadService): () => Promise<any>{
return () => appLoadService.intializeDestinosViajesState();
}
@Injectable()
class AppLoadService{
  constructor(private store: Store<AppState>, private http: HttpClient){}
  async intializeDestinosViajesState():Promise<any>{
    const headers: HttpHeaders = new HttpHeaders({'X-API-TOKEN':'token-seguridad'});
    const req = new HttpRequest('GET', APP_CONFIG_VALUE.apiEndpoint + '/my', {headers:headers})
    const response:any = await this.http.request(req).toPromise();
    this.store.dispatch(new InitMyDataAction(response.body));
  }
}
// fin init

//dexie db
export class Translation{
  constructor(public id: number, public lang: string, public key: string, public value:string){}
}
@Injectable({
  providedIn:'root'
})
export class MyDataBase extends Dexie{
  destinos: Dexie.Table<DestinoViaje, number>;
  translation: Dexie.Table<Translation, number>;
  constructor(){
    super('MyDataBase');
    this.version(1).stores({
      destinos: '++id, nombre, imagenURL', 
    });
    this.version(2).stores({
      destinos: '++id, nombre, imagenURL', 
      translation:'++id, lang, key, value'
    });
  }
}
export const db = new MyDataBase();
//fin dixie

//i18n init
class TranslationLoader implements TranslateLoader{
  constructor(private http: HttpClient){}

  getTranslation(lang: string ): Observable<any> {
    const promise = db.translation
            .where('lang')
            .equals(lang)
            .toArray()
            .then(results => {
              if (results.length ===0){
                    return this.http
                    .get<Translation[]>(APP_CONFIG_VALUE.apiEndpoint +'/api/translation?lang=' + lang)
                    .toPromise()
                    .then(apiResults => {
                      db.translation.bulkAdd(apiResults);
                      return apiResults;
                    });
                  }
                  return results;
                }).then((traducciones) => {
                  console.log('traducciones cargadas');
                  console.log(traducciones);
                  return traducciones;
                }).then((traducciones) => {
                  return traducciones.map((t) => ({[t.key]: t.value }));
                });
                return from(promise).pipe(flatMap((elems) => from(elems)));
    }
}
function HttpLoaderFactory(http: HttpClient){
  return new TranslationLoader(http);
}
//redux

@NgModule({
  declarations: [
    AppComponent,
    ListaComponent,
    ListaDestinoComponent,
    DetalleDestinoComponent,
    FormDestinoComponent,
    LoginComponent,
    ProtectedComponent,
    VuelosComponentComponent,
    VuelosMainComponentComponent,
    VuelosMasInfoComponentComponent,
    VuelosDetalleComponentComponent,
    EspiameDirective,
    TrackearClickDirective
  ],
  imports: [
    AppComponent, 
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    HttpClientModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes),
    NgRxStoreModule.forRoot(reducers, { initialState: reducersInitialState,
      runtimeChecks: {
        strictStateImmutability: false,
        strictActionImmutability: false,
      },
    }),
    EffectsModule.forRoot([DestinosViajesEffects]), 
    StoreDevtoolsModule.instrument(), 
    ReservasModule, 
    TranslateModule.forRoot({
      loader:{
        provide: TranslateLoader,
        useFactory: (HttpLoaderFactory), 
        deps: [ HttpClient]
      }
    }), 
    NgxMapboxGLModule,
    BrowserAnimationsModule
  ],
  providers: [
    DestinoApiCliente, 
    AuthService, 
    UsuarioLogueadoGuard, 
    {provide: APP_CONFIG, useValue:APP_CONFIG_VALUE}, 
    AppLoadService, 
    {provide: APP_INITIALIZER, useFactory: init_app, deps:[AppLoadService], multi: true}, 
    MyDataBase
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
