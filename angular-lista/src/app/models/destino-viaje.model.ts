export class DestinoViaje{

    public bandera: boolean;
    public servicio:string[];
    
    constructor(public nombre:string,public imagenURL:string, public num:number=0){
        this.servicio=['Piscina','Jacuzzi','Vehiculo']
    }

    isSeleccionado():boolean{
        return this.bandera;
    }

    setSeleccionado(s:boolean){
        this.bandera = s;
    }

    voteUp(){
        this.num++;
    }

    voteDown(){
        this.num--;
    }
}