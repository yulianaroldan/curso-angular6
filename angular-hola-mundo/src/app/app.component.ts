import { Component } from '@angular/core';

@Component({
  //Nombre del tag que es el que esta en el index
  selector: 'app-root',
  //donde se levanta el template
  templateUrl: './app.component.html',
  //donde se levantan los estilos
  styleUrls: ['./app.component.css']
})
//Se define el comportamiento encapsulado del componente
export class AppComponent {
  title = 'angular-hola-mundo';
}
